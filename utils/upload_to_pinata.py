"""
Upload files to Pinata
"""

import os
import requests
from utils.helpful_scripts import get_all_files
from config import PINATA_API_KEY, PINATA_SECRET_API_KEY


def upload_to_pinata(directory, folder_name):

    all_files = get_all_files(directory)
    files = [('file', (folder_name + '/' + os.path.basename(file),
              open(file, "rb"))) for file in all_files]

    headers = {
        'pinata_api_key': str(PINATA_API_KEY),
        'pinata_secret_api_key': str(PINATA_SECRET_API_KEY),
    }
    ipfs_url = "https://api.pinata.cloud/pinning/pinFileToIPFS"
    try:
        response = requests.post(url=ipfs_url, files=files, headers=headers)
        response_json = response.json()
        if len(all_files) == 1:
            response_json["filename"] = all_files[0].split('/')[-1]
        return response_json
        
    except Exception as e:
        return "error uploading images to pinata: " + str(e)

"""
Scripts used by the main code
"""

import os
from pathlib import Path

"""
Get a list of extensions for all files in the directory
"""
def get_extensions(directory):
    extensions=[]
    for file in os.listdir(directory):
        extensions.append(file.split('.')[-1])
    return extensions

"""
rename all images in the directory to 1.{extension},2.{extension}, etc
"""
def rename_images(directory):
    number_of_files = len(os.listdir(directory))
    # if more than one file, rename files
    if number_of_files > 1:
        file_number = 0
        # go through each filename sorted by name and rename it
        for filename in sorted(Path(directory).iterdir()):
            file_number += 1
            # get file extension
            old_filename, file_extension = os.path.splitext(filename)
            # generate new name
            new_name = os.path.join(directory,str(file_number) + file_extension)
            # Rename the file
            os.rename(filename, new_name)
    # if only one file, no need to rename
    elif number_of_files == 1:
        return
    # if no files, throw an error
    else:
        raise Exception(f"The folder {directory} has no files")


"""
Get a list of absolute paths to all the files located in the directory
"""
def get_all_files(directory):
    paths = []
    for root, dirs, files in os.walk(os.path.abspath(directory)):
        for file in files:
            paths.append(os.path.join(root, file))
    return paths
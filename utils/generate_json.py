"""
Generate json files of each one of the images already uploaded to Pinata
"""

import json
import os
import shutil
from config import FIRST_NAMES, LAST_NAMES, ATTRIBUTE_NAMES, ATTRIBUTE_LIST, ATTRIBUTE_DISPLAY_TYPE, EXT_URL
from utils.helpful_scripts import get_extensions

# Directory to save the json
json_dir = './json/'

"""
Add attributes function
To add attributes to the json file
"""
def _add_attribute(existing, attribute_name, options, token_id, display_type=None):
    trait = {
        'trait_type': attribute_name,
        'value': options[token_id % len(options)]
    }
    if display_type:
        trait['display_type'] = display_type
    existing.append(trait)


"""
Generate the json files of each one of the images
"""
def generate_json(ipfs_car, image_dir):
    # remove directory and files, then create again the directory
    if os.path.isdir(json_dir):
        shutil.rmtree(json_dir)
    os.makedirs(json_dir)
    # NFT JSON generation
    extensions = get_extensions(image_dir)
    if len(extensions) > 0:
        for token_id in range(len(extensions)):
            token_id = token_id + 1
            num_first_names = len(FIRST_NAMES)
            num_last_names = len(LAST_NAMES)
            freedom_name = "%s %s" % (
                FIRST_NAMES[token_id % num_first_names], LAST_NAMES[token_id % num_last_names])
            external_url = EXT_URL[token_id - 1]
            # Add attributes "randomnly"
            attributes = []
            i = 0
            for i in range(len(ATTRIBUTE_NAMES)):
                _add_attribute(attributes, ATTRIBUTE_NAMES[i], ATTRIBUTE_LIST[i],
                               token_id, display_type=ATTRIBUTE_DISPLAY_TYPE[i])
                i = i + 1
            # if only one file, generate the filename = <filename w/o extension>.json
            if len(extensions) == 1:
                image_filename = os.listdir(image_dir)[0]
                filename = os.path.join(
                    json_dir, image_filename.split('.')[0])
            # if multiple files, generate the filename = token_id + json
            else:
                image_filename = str(token_id) + "." + extensions[token_id - 1]
                filename = os.path.join(json_dir, str(token_id))
            # Assemble the json file containing:
            #   name, description, external_url, image_uri and list of attributes
            jsonString = json.dumps({
                'name': freedom_name,
                'description': "Freedom pictures",
                'external_url': external_url,
                'image': 'https://ipfs.io/ipfs/' + ipfs_car + '/' + image_filename,
                'attributes': attributes
            })
            jsonFile = open(filename, "w")
            jsonFile.write(jsonString)
            jsonFile.close()
        return
    else:
        raise Exception(f"The folder {image_dir} has no files")

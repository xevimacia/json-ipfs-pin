"""
Upload NFT images, create NFT json files and upload both to Pinata
nft_creation arguments:
    img_directory: location of the images in local drive
    folder: name of the folder where pinata will save images and json files
NOTE:
- If one image, the code keeps the original filename for both image and json file
- If multiple images,
    copy_and_overwrite will copy the original images to img_multiple folder
    nft_creation will call rename_images to rename the images
        from 1..n before uploading them to pinata
    json files will also be named from 1..n
"""

import os
import shutil

from utils.helpful_scripts import rename_images
from utils.upload_to_pinata import upload_to_pinata
from utils.generate_json import generate_json

def copy_and_overwrite(from_path, to_path):
    if os.path.exists(to_path):
        shutil.rmtree(to_path)
    shutil.copytree(from_path, to_path)

def nft_creation(img_directory, folder):
    # json files folder
    json_directory = 'json'
    # rename images in the directory - when multiple files
    rename_images(img_directory)
    # upload images to pinata
    print('\nSTEP 1: Uploading images to Pinata. Please wait...')
    images_json = upload_to_pinata(img_directory, folder + '_images')
    print('Images successfully uploaded to Pinata')
    http_image_tail = f"{images_json['IpfsHash']}/"
    if 'filename' in images_json:
        http_image_tail = f"{http_image_tail}{images_json['filename']}"
    print('\nIf you are using Brave or Puma browser, images can be found here:')
    print(f'ipfs://{http_image_tail}')
    print(" \nElse, use one of the following gateways:")
    print("Open images from pinata gateway:" )
    print(f"https://gateway.pinata.cloud/ipfs/{http_image_tail}" )
    print(f"Open images from ipfs gateway:" )
    print(f"https://ipfs.io/ipfs/{http_image_tail}" )
    # generate json
    generate_json(ipfs_car=images_json['IpfsHash'], image_dir=img_directory)
    # upload json file to pinata
    print('\nSTEP 2: Uploading json files to Pinata. Please wait...')
    json_files_json = upload_to_pinata(json_directory, folder + '_json')
    print('Json files successfully uploaded to Pinata')
    http_image_tail = f"{json_files_json['IpfsHash']}/"
    if 'filename' in json_files_json:
        http_image_tail = f"{http_image_tail}{json_files_json['filename']}"
    print('\nIf you are using Brave or Puma browser, json files can be found here:')
    print(f'ipfs://{http_image_tail}')
    print(" \nElse, use one of the following gateways:")
    print("Open json files from pinata gateway:" )
    print(f"https://gateway.pinata.cloud/ipfs/{http_image_tail}" )
    print(f"Open json files from ipfs gateway:" )
    print(f"https://ipfs.io/ipfs/{http_image_tail}" )

if __name__ == "__main__":
    """
    Create a single image NFT
    nft_creation arguments:
        img_one = 'img_one'
        folder_one = 'jason_hogan' - the owner of the image
    """
    img_one = 'img_one'
    folder_one = 'jason_hogan'
    print("\n***** Creating ONE NFT *****")
    nft_creation(img_directory = img_one, folder = folder_one)
    
    """
    Create a multiple image NFTs
    nft_creation arguments:
        img_multiple = 'img_multiple'
        folder_multiple = 'freedom'
    """
    img_multiple = 'img_multiple'
    folder_multiple = 'freedom'
    # (Overwrites) copy the directory img_original to the directory img_multiple
    copy_and_overwrite(from_path='img_original', to_path=img_multiple)
    print("\n***** Creating MULTIPLE NFTs *****")
    nft_creation(img_directory = img_multiple, folder = folder_multiple)

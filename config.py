'''
Config file for evironment variables
'''
import os
from dotenv import load_dotenv

load_dotenv()

"""
Create a Pinata account and API keys
API keys are stored in .env file with:
    export PINATA_API_KEY={your public key here}
    export PINATA_SECRET_API_KEY={your secret key here}
"""
PINATA_API_KEY = os.getenv("PINATA_API_KEY")
PINATA_SECRET_API_KEY = os.getenv("PINATA_SECRET_API_KEY")


'''
List to generate a random name to each NFT
'''
FIRST_NAMES = ['Free', 'Release', 'Unchain', 'Let go', 'Set Lose']
LAST_NAMES = ['me', 'us', 'everyone', 'my life']

'''
Attributes to be edited manually
NOTE: The attributes above could be retrieved from a db (in production)
or an excel file (to be more human readable). For simplicity.
Used 8 lists of hard-coded attributes in the example.
'''
FEELINGS = ['flying', 'climbing', 'floating',
            'spinning', 'dreaming', 'euphoria']
WILLIGNESS = ['1', '2', '3', '4', '5']
INT_ATTRIBUTES = [5, 2, 3, 4, 8]
FLOAT_ATTRIBUTES = [1.4, 2.3, 11.7, 90.2, 1.2]
STR_ATTRIBUTES = [
    'thriving',
    'crazy',
    'darkness',
    'boring',
    'dying'
]
BOOST_ATTRIBUTES = [10, 40, 30]
PERCENT_BOOST_ATTRIBUTES = [5, 10, 15]
NUMBER_ATTRIBUTES = [1, 2, 3, 4]

'''
Lists of lists of attributes - in our example 8 attributes' lists
'''
# List of attribute's names - in this case we have 8 per NFT
ATTRIBUTE_NAMES = ['feelings', 'willigness', 'level', 'stamina',
                   'status', 'force', 'stamina_increase', 'generation']
# List of attribute's values (from lists above) - to be picked 'randomly' at generation
ATTRIBUTE_LIST = [FEELINGS, WILLIGNESS, INT_ATTRIBUTES, FLOAT_ATTRIBUTES,
                  STR_ATTRIBUTES, BOOST_ATTRIBUTES, PERCENT_BOOST_ATTRIBUTES, NUMBER_ATTRIBUTES]
# List of attribute's display type
ATTRIBUTE_DISPLAY_TYPE = [None, None, None, None,
                          None, 'boost_number', 'boost_percentage', 'number']

'''
External url corresponds to the image downloaded from https://unsplash.com
We must give credit to the great photographers that created those images
'''
EXT_URL = ['https://unsplash.com/photos/YyFwUKzv5FM', 'https://unsplash.com/photos/IZrEEtu3fkk',
           'https://unsplash.com/photos/zh0J32MrJfA', 'https://unsplash.com/photos/c77dIthd_Tk']

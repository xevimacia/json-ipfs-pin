# Pin NFT image/json to IPFS


This is a Python code to:
1. Pin images to IPFS via Pinata
2. Generate the corresponding json files with its rarities
3. Pin json files to IPFS via Pinata

The code is not perfect but can be a great start to create your own IPFS files and use them to deploy your NFT smart contract.

If you have only one image, it keeps the image filename untouched.

If you have multiple images, it copies and renames the images from 1 to n number of files.
___
## Installation

If you are cloning the project then run this first, otherwise you can download the source code on the release page and skip this step.

```bash
git clone git@gitlab.com:xevimacia/json-ipfs-pin.git
```

### Project tree

Below are the files used in the project:
 * [img_multiple](./img_multiple)
   * *.jpg or *.png
 * [img_one](./img_one)
   * *.jpg or *.png
 * [img_original](./img_original)
   * *.jpg or *.png
 * [json](./json)
   * *.json
 * [utils](./utils)
   * [generate_json.py](./utils/generate_json.py)
   * [helpful_scripts.py](./utils/helpful_scripts.py)
   * [upload_to_pinata.py](./utils/upload_to_pinata.py)
 * [config.py](./config.py)
 * [dot_env](./dot_env) - rename to .env
 * [nft_creation.py](./nft_creation.py)
 * [README.md](./README.md)
### Get Pinata account ready
Follow the link below to create a free Pinata account: https://www.pinata.cloud/

Also create your API keys as explained in the following link: https://docs.pinata.cloud/

Rename [dot_env](./dot_env) to `.env` and add the newly generated Pinata API keys.
### Packages
This project uses the following packages - most of them ready available with Python 3.8 and above:
[os](https://docs.python.org/3/library/os.html), [shutil](https://docs.python.org/3/library/shutil.html), [pathlib](https://docs.python.org/3/library/pathlib.html), [dotenv](https://github.com/theskumar/python-dotenv), [requests](https://requests.readthedocs.io/en/latest/), [json](https://docs.python.org/3/library/json.html)
___
## Usage
You can quickly run the project as it is. It will generate a single NFT and multiple NFTs (4 of them).

Otherwise, if you wish to use it for your own NFTs, below are the steps you should follow to use this code:

1. Copy your images to either to:
- [img_one](./img_one) - for one NFT

  or
- [img_original](./img_original) - for multiple NFTs.

2. Edit the names located at [config.py](./config.py).
```python
'''
Attributes to be edited manually
Used 8 lists of hard-coded attributes in the example.
'''
FEELINGS = ['flying', 'climbing', 'floating', 'spinning', 'dreaming', 'euphoria']
WILLIGNESS = ['1', '2', '3', '4', '5']
INT_ATTRIBUTES = [5, 2, 3, 4, 8]
FLOAT_ATTRIBUTES = [1.4, 2.3, 11.7, 90.2, 1.2]
STR_ATTRIBUTES = ['thriving', 'crazy', 'darkness', 'boring', 'dying']
BOOST_ATTRIBUTES = [10, 40, 30]
PERCENT_BOOST_ATTRIBUTES = [5, 10, 15]
NUMBER_ATTRIBUTES = [1, 2, 3, 4]
```
- `Note`: The attributes above could be retrieved from a db (in production) or an excel file (to be more human readable). For simplicity, I used hard-coded attributes in the example.
3. Edit the lists of attribute fields. To be edited in case you modified the attribute lists above.
```python
'''
Lists of lists of attributes - in our example 8 attributes' lists
'''
# List of attribute's names - in this case we have 8 per NFT
ATTRIBUTE_NAMES = ['feelings', 'willigness', 'level', 'stamina', 'status', 'force', 'stamina_increase', 'generation']
# List of attribute's values (from lists above) - to be picked 'randomly' at generation
ATTRIBUTE_LIST = [FEELINGS, WILLIGNESS, INT_ATTRIBUTES, FLOAT_ATTRIBUTES, STR_ATTRIBUTES, BOOST_ATTRIBUTES, PERCENT_BOOST_ATTRIBUTES, NUMBER_ATTRIBUTES]
# List of attribute's display type
ATTRIBUTE_DISPLAY_TYPE = [None, None, None, None, None, 'boost_number', 'boost_percentage', 'number']
```
4. In [nft_creation.py](./nft_creation.py), edit as follows:
- If you want to create only one NFT, comment out or delete the following code:
```python
  """
  Create a multiple image NFTs
  nft_creation arguments:
    img_multiple = 'img_multiple'
    folder_multiple = 'freedom'
  """
  img_multiple = 'img_multiple'
  folder_multiple = 'freedom'
  # (Overwrites) copy the directory img_original to the directory img_multiple
  copy_and_overwrite(from_path='img_original', to_path=img_multiple)
  print("\n***** Creating MULTIPLE NFTs *****")
  nft_creation(img_directory = img_multiple, folder = folder_multiple)
```
- If you want to create multiple NFTs, comment out or delete the following code: 
```
"""
  Create a single image NFT
  nft_creation arguments:
    img_one = 'img_one'
    folder_one = 'jason_hogan' - the owner of the image
  """
  img_one = 'img_one'
  folder_one = 'jason_hogan'
  print("\n***** Creating ONE NFT *****")
  nft_creation(img_directory = img_one, folder = folder_one)
```
1. Run `nft_creation.py` to create the NFTs' images and json files.
  ```python
  python3 nft_creation.py
  ```
2. If you don't have any errors, you should have an output like below:
```python
***** Creating ONE NFT *****

STEP 1: Uploading images to Pinata. Please wait...
Images successfully uploaded to Pinata

If you are using Brave or Puma browser, images can be found here:
ipfs://QmWJt5o5VxZPo3fUKHD7msSab4KiJmrihme1mkzj7R3EQc/jason-hogan-YyFwUKzv5FM-unsplash.jpg
 
Else, use one of the following gateways:
Open images from pinata gateway:
https://gateway.pinata.cloud/ipfs/QmWJt5o5VxZPo3fUKHD7msSab4KiJmrihme1mkzj7R3EQc/jason-hogan-YyFwUKzv5FM-unsplash.jpg
Open images from ipfs gateway:
https://ipfs.io/ipfs/QmWJt5o5VxZPo3fUKHD7msSab4KiJmrihme1mkzj7R3EQc/jason-hogan-YyFwUKzv5FM-unsplash.jpg

STEP 2: Uploading json files to Pinata. Please wait...
Json files successfully uploaded to Pinata

If you are using Brave or Puma browser, json files can be found here:
ipfs://QmYdyX8mhCCorGfFmBiNYshjQKPUuw3Ho3Lq2BHytRp3Mc/jason-hogan-YyFwUKzv5FM-unsplash
 
Else, use one of the following gateways:
Open json files from pinata gateway:
https://gateway.pinata.cloud/ipfs/QmYdyX8mhCCorGfFmBiNYshjQKPUuw3Ho3Lq2BHytRp3Mc/jason-hogan-YyFwUKzv5FM-unsplash
Open json files from ipfs gateway:
https://ipfs.io/ipfs/QmYdyX8mhCCorGfFmBiNYshjQKPUuw3Ho3Lq2BHytRp3Mc/jason-hogan-YyFwUKzv5FM-unsplash

```
___
## View files
If you use Brave or Puma browsers (as many crypto enthusiasts do) you can view the pinned files with `ipfs://{CID}`. For example, one NFT:
- Json file: `ipfs://QmYdyX8mhCCorGfFmBiNYshjQKPUuw3Ho3Lq2BHytRp3Mc/jason-hogan-YyFwUKzv5FM-unsplash`
- It's corresponding image: `ipfs://QmWJt5o5VxZPo3fUKHD7msSab4KiJmrihme1mkzj7R3EQc/jason-hogan-YyFwUKzv5FM-unsplash.jpg`
  
    `Note`: We are using the ipfs gateway as image link in the json file, read below.

However, if you use any other browser you have to use a gateway. Gateways have an important downside that has to be considered. Links could stop working in the future and so if used, the image could fail to appear, even if it's hosted on the network.

The most used gateways are https://ipfs.io and https://gateway.pinata.cloud.

You can view the pinned files for multiple NFTs as below:
- Json folder:
  - https://ipfs.io/ipfs/QmUdswkJmZaLPcdUKDv6J5QrqL4RubFxtkVNuWYKwerg4G/
  - https://gateway.pinata.cloud/ipfs/QmUdswkJmZaLPcdUKDv6J5QrqL4RubFxtkVNuWYKwerg4G/ 
- Corresponding image folder:
  - https://ipfs.io/ipfs/QmRxgAHQv8Hw3ku1sW7kTL2yWzEkjZGQFJYymzFpgjrxzc/
  - https://gateway.pinata.cloud/ipfs/QmRxgAHQv8Hw3ku1sW7kTL2yWzEkjZGQFJYymzFpgjrxzc/

`Note:` As of today, gateways are the most popular method to link your files. However, keep in mind that the gateway link may no longer work in the future.
`Note 2:` To have a compatible json file in Opensea, do not give `.json` extension to your files.
___
## Photo Credits
I added external url to the json file corresponding to the image downloaded from https://unsplash.com.

We must give credit to the photographers that took those amazing pictures
```python
EXT_URL = ['https://unsplash.com/photos/YyFwUKzv5FM', 'https://unsplash.com/photos/IZrEEtu3fkk', 'https://unsplash.com/photos/zh0J32MrJfA', 'https://unsplash.com/photos/c77dIthd_Tk']
```
___
## Writen

entirely in Python
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
___
## License
[MIT](https://choosealicense.com/licenses/mit/)
